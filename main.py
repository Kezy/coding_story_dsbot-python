import discord
from discord.ext import commands
from config import settings
from youtube_dl import YoutubeDL
import asyncio


client = commands.Bot(command_prefix=settings['prefix'])  # создание "тела" бота


urls = []
durations = []
titles = []
web_urls = []
icon_url = []
channels = []


async def player(ctx):
    global urls, durations, titles, icon_url

    voice_client = discord.utils.get(client.voice_clients, guild=ctx.guild)
    ffmpeg_options = {
        'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}

    if not voice_client.is_playing():
        while urls:
            if not voice_client.is_playing():
                voice_client.play(discord.FFmpegPCMAudio(urls[0], **ffmpeg_options))

                await asyncio.sleep(durations[0])



@client.command()
async def play(ctx, arg):

    # подключение в голосовой канал
    voice = ctx.message.author.voice
    voice_client = discord.utils.get(client.voice_clients, guild=ctx.guild)
    if not voice_client:
        if voice:
            await voice.channel.connect()
        else:
            await ctx.send('для использования бота нужно быть в голосовом канале')
            return

    ydl_options = {'format': 'bestaudio', 'noplaylist': 'True', 'download': 'False'}

    video = YoutubeDL(ydl_options).extract_info(arg, download=False)

    url = video['url']
    title = video['title']
    duration = video['duration']
    web_url = video['webpage_url']
    thumbnail = video['thumbnail']

    channels.append(video['channel'])
    icon_url.append(thumbnail)
    web_urls.append(web_url)
    durations.append(duration)
    titles.append(title)
    urls.append(url)

    await player(ctx)

client.run(settings['token'])
